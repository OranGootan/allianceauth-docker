I put together a Docker image for AllianceAuth and a Docker-Compose stack just follow the Following:


1.  run `git clone https://gitlab.com/antt1995/allianceauth-docker`
2.  edit the conf/local.py for your eve api and mysql info and reflect the mysql info in docker-compose.yml
3.  run `docker-compose up -d`
4.  run `docker-compose logs -f --tail=20` When the images have finished downloading and are running continue
5.  run `docker exec -it aa-auth python manage.py createsuperuser`
And your done!



Im also putting together a bootstrap script but its not ready yet
