# Every setting in base.py can be overloaded by redefining it here.
from .base import *
# Django App Secretkey change if you want
SECRET_KEY = '#5hbuczgvymg8i!$#ewlwbtzzjku3_5j^)*@=eqxm&pawxnw*+'
# Change this to change the name of the auth site displayed in page titles and $
SITE_NAME = 'myauth'
# Change this to enable/disable debug mode, which displays useful error message$
# sensitive data.
DEBUG = False
# Add any additional apps to this list. Pre-Populated with some Apps
INSTALLED_APPS += [
'allianceauth.eveonline.autogroups',
'allianceauth.hrapplications',
'allianceauth.corputils',
'allianceauth.fleetactivitytracking',
'allianceauth.optimer',
'allianceauth.srp',
'allianceauth.timerboard',
]
# Enter credentials to use MariaDB. Please Reflect Changes In the docker-compose.yml
DATABASES['default'] = {
    'ENGINE': 'django.db.backends.mysql',
    'NAME': 'aauth',
    'USER': 'aauth',
    'PASSWORD': 'qwerty',
    'HOST': 'mariadb',
    'PORT': '3306',
}
# Register an application at https://developers.eveonline.com for Authenticatio$
# out these settings. Be sure to set the callback URL to https://example.com/ss$
# your domain for example.com Logging in to auth requires the publicData scope $
# through the LOGIN_TOKEN_SCOPES setting). Other apps may require more (see the$
ESI_SSO_CLIENT_ID = 'KEY HERE'
ESI_SSO_CLIENT_SECRET = 'SECRET HERE'
ESI_SSO_CALLBACK_URL = 'http://URL HERE/sso/callback'
# By default emails are validated before new users can log in. It's recommended$
# like SparkPost or Elastic Email to send email. https://www.sparkpost.com/docs$
# https://elasticemail.com/resources/settings/smtp-api/ Set the default from em$
# 'noreply@example.com' Email validation can be turned off by uncommenting the $
# break some services. REGISTRATION_VERIFY_EMAIL = False
EMAIL_HOST = ''
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = ''
#######################################
# Add any custom settings below here. #
#######################################



#######################################
# DO NOT CHANGE ANYTHING BELOW HERE.  #
#######################################
ROOT_URLCONF = 'myauth.urls'
WSGI_APPLICATION = 'myauth.wsgi.application'
STATIC_ROOT = "/var/www/myauth/static/"
BROKER_URL = 'redis://redis:6379/0'
CELERY_RESULT_BACKEND = 'redis://redis:6379/0'
CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": "redis:6379",
        "OPTIONS": {
            "DB": 1,
        }
    }
}

